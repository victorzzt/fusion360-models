#### IRBlockingSensorMount
* A mount for ir pair sensor like this: https://img.ruten.com.tw/s2/5/a8/fc/22010319138044_696.jpg (Retrieved 20200923)

#### FlappingCardboard
* Cranking flapping mechanism mount constructed using cardboard(lasercut+glue)

#### PowerbankDigiSparkATTiny85
* A clipper for clipping a digispark ATTiny85 (microusb varient https://hackster.imgix.net/uploads/attachments/883904/FS3Z3YNJUSNE16Y.png?auto=compress%2Cformat&w=900&h=675&fit=min ,Retrieved 20200923) on a DIY powerbank kit like (https://i2.wp.com/ae01.alicdn.com/kf/Hfd2ce82997734584834280a11fab52d85/Hot-Sell-18650-Battery-DIY-USB-Power-Bank-case-Kit-1x-18650-Holder-Case-for-Battery.jpg , Retrieved 20200923, 18650 square)

#### DIYToyTrain
* A toy train that's mostly 3d printed that can run on Tomy Track, More detail here: (https://wiki.melissakronenberger.com/bin/view/Main/Past%20works/3D%20printed%20toy%20train/ , will also include the extra parts you'll need) Made for Makerfaire Hongkong 2018, thanks to Hongkong Maker Faire and Assist. Prof. Clifford Choy

#### SliderWCabornStick
* A slider that can slide 2 potentiometers and a microswitch (for multi-model input, a part of this installation: https://dl.acm.org/doi/10.1145/3294109.3295649)

#### HandleOfLightSaber
* Handle of light saber with acrylic tube (Unfinished, can have electronics inside)

#### CabornFiberStickBuildingBlock
* Connector for building prototype using 2x2mm caborn fibre stick (square)

#### ThreeNeoPixelClip
* A clip that can hold 3 neopixels (with PCB extension), good for small alarm/traffic light projects

#### LaserDiodeMount
* Mount for cheap laser diode (those that have copper shell)

#### NTPClock
* A clock that uses ESP8266, need a 24 pixel and a 12 pixel. I don't have the program, but it's not that hard :)

#### ThreeWayNeopixel
* A guide where you can stick neopixel onto 3 sides to make it omni light, Note it's built for 3535 SK6812 strips, NOT the traditional 5050 WS2812/SK6812

#### YG2900Mount
* As title, built for demo purpose, probably not that useful

#### TinkerboardMount
* A tinkerboard monitor mount, it mounts a tinkerboard onto a monitor, has heatsink, fan installation guide. As it's hard coded, it wouldn't be that useful other than a design reference

#### NodeMCU12EDocking
* A box for NodeMCU-12E extension board (Servo plug style, has extra 5V from outside to prevent from diode burn, but not that intuitive to use), To fabricate the PCB, please goto: https://bitbucket.org/victorzzt/nodemcu-v3-docking-board/src/master/release/ ,source files is in Eagle (Might need to change to other free tools and remake soon too, sucks to be poor :P) Also thanks to Hongkong Maker Faire and Assist Prof. Clifford Choy

